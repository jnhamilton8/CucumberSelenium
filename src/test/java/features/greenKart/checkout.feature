Feature: Search and place the order for products

  @PlaceOrder
  Scenario Outline: Search Experience for product search in home and office page
    Given User is on GreenCart landing page
    When User searches with shortname <Name> on Home page
    And Added "3" items of the selected product to cart
    Then User proceeds to checkout and validates the <Name> items in the checkout page
    And Verify the user has the ability to enter the promo code and place the order
    Examples:
      | Name |
      | Cauliflower  |