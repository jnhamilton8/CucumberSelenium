Feature: Search and place the order for products

  @OffersPage
  Scenario Outline: Search Experience for product search in home and office page
    Given User is on GreenCart landing page
    When User searches with shortname <Name> on Home page
    Then User searches for <Name> in Offers page
    And Product name exists in Offers page
    Examples:
      | Name |
      | Tom  |
      | WillFail |