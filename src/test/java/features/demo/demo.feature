Feature: Application Login

  Background:
    Given Setup the entries in database
    When Launch the browser from config variables
    And Hit the home page url of banking site

  @Demo @NetBanking
  Scenario Outline: Logging in
    Given User is on NetBanking landing page
    When User login into application with <username> and password <password>
    Then Home Page is displayed
    And Cards are displayed
    Examples:
      | username  | password |
      | user      | 1234 |
      | admin     | 4567 |

  @Demo @NetBanking
  Scenario: User Page default Sign up
    Given User is on Practice landing page
    When User registers on application
      | rahul |
      | shetty |
      | contact@email.com |
      | 54343646463 |
    Then Home Page is displayed
    And Cards are displayed