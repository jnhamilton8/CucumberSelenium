Feature: Application Login Second Program

  Background:
    When Launch the browser from config variables
    And Hit the home page url of banking site

  @Demo @NetBanking
  Scenario: User Page default login
    Given User is on NetBanking landing page
    When User login into application with "user" and password 0953
    Then Home Page is displayed
    And Cards are displayed


  @Demo @Mortgage
  Scenario Outline: Mortgage User Page default login
    Given User is on NetBanking landing page
    When User login into application with "<Username>" and password "<Password>"
    Then Home Page is displayed
    And Cards are displayed

    Examples:
      | Username	 | Password |
      | debituser |  hello12 |
      | credituser |  lpo213  |