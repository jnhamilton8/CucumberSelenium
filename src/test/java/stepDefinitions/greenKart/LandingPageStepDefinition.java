package stepDefinitions.greenKart;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import pageObjects.LandingPage;
import utils.TestContextSetup;

public class LandingPageStepDefinition {
    TestContextSetup testContextSetup;
    LandingPage landingPage;

    public LandingPageStepDefinition(TestContextSetup testContextSetup) {
        this.testContextSetup = testContextSetup;
        this.landingPage = testContextSetup.pageObjectManager.getLandingPage();
    }

    @Given("User is on GreenCart landing page")
    public void user_is_on_green_cart_landing_page() {
        landingPage.waitUntilLandingPageIsLoaded();
    }

    @When("^User searches with shortname (.+) on Home page$")
    public void user_searches_with_shortname_on_home_page(String shortName) {
        landingPage.enterSearchItem(shortName);
        testContextSetup.productNameSearchPage = landingPage.returnProductName().split("-")[0].trim();
    }

    @When("Added {string} items of the selected product to cart")
    public void added_items_of_the_selected_product_to_cart(String numItems) {
        landingPage.incrementNumOfFirstItem(Integer.parseInt(numItems));
        landingPage.clickAddToCartOnFirstItem();
    }
}