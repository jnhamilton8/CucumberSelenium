package stepDefinitions.greenKart;

import io.cucumber.java.en.Then;
import pageObjects.CheckoutPage;
import utils.TestContextSetup;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CheckoutPageStepDefinition {

    private final CheckoutPage checkoutPage;
    TestContextSetup testContextSetup;

    public CheckoutPageStepDefinition(TestContextSetup testContextSetup) {
        this.testContextSetup = testContextSetup;
        this.checkoutPage = testContextSetup.pageObjectManager.getCheckoutPage();
    }

    @Then("^User proceeds to checkout and validates the (.+) items in the checkout page$")
    public void user_proceeds_to_checkout_and_validates_the_items_in_the_checkout_page(String itemPutInCart) {
        checkoutPage.checkoutItems();

        var productNameOnCartPage = checkoutPage.returnProductName();
        assertThat(productNameOnCartPage, equalTo(itemPutInCart));

        var numOfItemsInCart = checkoutPage.returnNumOfItemInCart();
        assertThat(numOfItemsInCart, equalTo(3));
    }

    @Then("Verify the user has the ability to enter the promo code and place the order")
    public void verify_the_user_has_the_ability_to_enter_the_promo_code_and_place_the_order() {
        assertThat(checkoutPage.isPromoButtonDisplayed(), equalTo(true));
        assertThat(checkoutPage.isPlaceOrderButtonPresent(), equalTo(true));
    }
}
