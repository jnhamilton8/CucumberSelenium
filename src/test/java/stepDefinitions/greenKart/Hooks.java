package stepDefinitions.greenKart;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import utils.TestContextSetup;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Hooks {

    private final TestContextSetup testContextSetup;

    public Hooks(TestContextSetup testContextSetup) {
        this.testContextSetup = testContextSetup;
    }

    @After
    public void afterScenario() {
        testContextSetup.testBase.getDriver().quit();
    }

    @AfterStep
    public void addScreenshot(Scenario scenario) {
        if(scenario.isFailed()) {
            try {
                File sourcePath = ((TakesScreenshot) testContextSetup.testBase.getDriver()).getScreenshotAs(OutputType.FILE);
                byte[] fileContentInBytes = Files.readAllBytes(sourcePath.toPath());
                scenario.attach(fileContentInBytes, "image/png", "image");
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
