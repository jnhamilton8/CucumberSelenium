package stepDefinitions.greenKart;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pageObjects.LandingPage;
import pageObjects.OffersPage;
import utils.TestContextSetup;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class OfferPageStepDefinition {

    TestContextSetup testContextSetup;
    LandingPage landingPage;
    OffersPage offersPage;

    public OfferPageStepDefinition(TestContextSetup testContextSetup) {
        this.testContextSetup = testContextSetup;
        this.landingPage = testContextSetup.pageObjectManager.getLandingPage();
        this.offersPage = testContextSetup.pageObjectManager.getOffersPage();
    }

    @Then("^User searches for (.+) in Offers page$")
    public void user_searches_for_product_in_offers_page(String shortName) {
        landingPage.clickTopDealsButton();
        switchToOffersPage();
        offersPage.searchFor(shortName);
    }

    @And("Product name exists in Offers page")
    public void product_name_exists_in_offers_Page() {
        var productNameOffersPage = offersPage.getProductName();
        assertThat(testContextSetup.productNameSearchPage, equalTo(productNameOffersPage));
    }

    public void switchToOffersPage() {
        if(!testContextSetup.genericUtils.doesUrlContain("offers")) {
            testContextSetup.genericUtils.switchToChildWindow();
        }
    }
}