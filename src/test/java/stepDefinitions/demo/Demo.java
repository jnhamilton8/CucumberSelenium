package stepDefinitions.demo;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;

public class Demo {
    @Given("User is on NetBanking landing page")
    public void user_is_on_net_banking_landing_page() {
        System.out.println("User landed on the netbanking page");
    }

    @Given("User is on Practice landing page")
    public void user_is_on_practice_landing_page() {
        System.out.println("User landed on practice landing page");
    }

    @Given("Setup the entries in database")
    public void setup_entries_in_database() {
        System.out.println("Set up entries in DB");
    }

    @When("Launch the browser from config variables")
    public void launch_the_browser_from_config_variables() {
        System.out.println("Browser is launched");
    }

    @When("^User login into application with (.+) and password (.+)$")
    public void user_login_into_application_with_and_password(String username, String password) {
        System.out.println(username + "and password is " + password);
    }

    @When("User registers on application")
    public void user_registers_on_application(List<String> dataTable) {
        var firstName = dataTable.get(0);
        var lastName = dataTable.get(1);
        var email = dataTable.get(2);
        var id = dataTable.get(3);
        System.out.printf("Firstname: %s, Surname: %s, Email: %s, Id: %s", firstName, lastName, email, id);
    }

    @Then("Hit the home page url of banking site")
    public void hit_the_home_page_url_of_banking_site() {
        System.out.println("Home page of Banking site hit");
    }

    @Then("Home Page is displayed")
    public void home_page_is_displayed() {
        System.out.println("Home page is displayed");

    }
    @Then("Cards are displayed")
    public void cards_are_displayed() {
        System.out.println("Cards are displayed");
    }

}
