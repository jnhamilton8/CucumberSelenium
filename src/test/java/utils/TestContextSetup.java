package utils;

import org.openqa.selenium.WebDriver;
import pageObjects.PageObjectManager;;

public class TestContextSetup {
    public WebDriver driver;
    public String productNameSearchPage;
    public PageObjectManager pageObjectManager;
    public GenericUtils genericUtils;
    public TestBase testBase;

    public TestContextSetup() {
        testBase = new TestBase();
        pageObjectManager = new PageObjectManager(testBase.getDriver(), this);
        genericUtils = new GenericUtils(testBase.getDriver());
    }
}
