package utils;

import org.openqa.selenium.WebDriver;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class GenericUtils {
    WebDriver driver;
    public GenericUtils(WebDriver driver) {
        this.driver = driver;
    }

    public void switchToChildWindow() {
        Set<String> windowHandles = driver.getWindowHandles();
        Iterator<String> iterator = windowHandles.iterator();
        var parentWindow = iterator.next();
        var childWindow = iterator.next();
        driver.switchTo().window(childWindow);
    }

    public void staticWaitFor(int millis) {
        try {
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean doesUrlContain(String aString) {
        return driver.getCurrentUrl().contains(aString);
    }
}
