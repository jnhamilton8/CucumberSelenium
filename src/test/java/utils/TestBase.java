package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.naming.ConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TestBase {

    public WebDriver driver;
    public WebDriver getDriver() {
        var url = getSystemOrLocalProperty("qaUrl");
        var browser = getSystemOrLocalProperty("browser");

        if(driver == null) {
            driver = returnDriver(browser);
            driver.get(url);
        }
        return driver;
    }

    public String getSystemOrLocalProperty(String key) {
        var value = System.getProperty(key);
        if (value != null) {
            return value;
        } else {
            try (FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "//src//test//resources//global.properties")) {
                Properties props = new Properties();
                props.load(fis);
                var propertyValue = props.getProperty(key);
                if (propertyValue != null) {
                    return props.getProperty(key);
                } else {
                    throw new WebDriverException(String.format("A valid property for %s must be provided", key));
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public WebDriver returnDriver(String browser) {
        WebDriver driver;
        switch(browser.toLowerCase()) {
            case "firefox" -> {
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
            }
            case "edge" -> {
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
            }
            default -> {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
            }
        }
        return driver;
    }
}
