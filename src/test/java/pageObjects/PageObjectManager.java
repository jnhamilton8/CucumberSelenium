package pageObjects;

import org.openqa.selenium.WebDriver;
import utils.TestContextSetup;

public class PageObjectManager {

    WebDriver driver;
    TestContextSetup testContextSetup;

    public PageObjectManager(WebDriver driver, TestContextSetup testContextSetup) {
        this.driver = driver;
        this.testContextSetup = testContextSetup;
    }

    public LandingPage getLandingPage(){
        return new LandingPage(driver, testContextSetup);
    }

    public OffersPage getOffersPage() {
        return new OffersPage(driver);
    }

    public CheckoutPage getCheckoutPage() { return new CheckoutPage(driver, testContextSetup); }
}