package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

public class OffersPage {

    WebDriver driver;

    public OffersPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By searchField = By.xpath("//input[@type='search']");
    private final By productName = By.cssSelector("tr td:nth-child(1)");

    public void searchFor(String item) {
        FluentWait<WebDriver> wait = returnFluentWait(driver, 2, 300);
        var searchBox = wait.until(ExpectedConditions.visibilityOfElementLocated(searchField));
        searchBox.sendKeys(item);
    }

    public String getProductName(){
        FluentWait<WebDriver> wait = returnFluentWait(driver, 2, 300);
        var productNameElement = wait.until(ExpectedConditions.visibilityOfElementLocated(productName));
        return productNameElement.getText();
    }

    public FluentWait<WebDriver> returnFluentWait(WebDriver driver, int durationInSec, int pollingInMs) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(durationInSec))
                .pollingEvery(Duration.ofMillis(pollingInMs))
                .ignoring(NoSuchElementException.class);
    }
}
