package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.TestContextSetup;

public class CheckoutPage {

    WebDriver driver;
    TestContextSetup testContextSetup;

    public CheckoutPage(WebDriver driver, TestContextSetup testContextSetup) {
        this.driver = driver;
        this.testContextSetup = testContextSetup;
    }

    private final By cartBag =  By. cssSelector("[alt='Cart']");
    private final By checkoutButton = By.xpath("//button[contains(text(), 'PROCEED TO CHECKOUT')]");
    private final By promoButton = By.cssSelector(".promoBtn");
    private final By placeOrderButton = By.xpath(("//button[contains(text(), 'Place Order')]"));
    private final By productName = By.cssSelector("p.product-name");
    private final By quantityOfItem = By.cssSelector("p.quantity");

    public void checkoutItems() {
        driver.findElement(cartBag).click();
        driver.findElement(checkoutButton).click();
        testContextSetup.genericUtils.staticWaitFor(2000);
    }

    public boolean isPromoButtonDisplayed() {
       return driver.findElement(promoButton).isDisplayed();
    }

    public boolean isPlaceOrderButtonPresent() {
        return driver.findElement(placeOrderButton).isDisplayed();
    }

    public String returnProductName() {
        var productNameText = driver.findElement(productName).getText().split("-")[0].trim();
        System.out.printf("Product name is: %s%n", productNameText);
        return productNameText;
    }

    public int returnNumOfItemInCart() {
        var quantityOfItemInCart = driver.findElement(quantityOfItem).getText();
        System.out.println("The extracted quantity is: " + quantityOfItemInCart);
        return Integer.parseInt(quantityOfItemInCart);
    }
}