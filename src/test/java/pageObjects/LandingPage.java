package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import utils.TestContextSetup;

import java.time.Duration;

public class LandingPage {

    WebDriver driver;
    TestContextSetup testContextSetup;

    public LandingPage(WebDriver driver, TestContextSetup testContextSetup) {
        this.driver = driver;
        this.testContextSetup = testContextSetup;
    }

    private final By searchField =  By.xpath("//input[@type='search']");
    private final By productName = By.cssSelector("h4.product-name");
    private final By topDealsButton = By.linkText("Top Deals");
    private final By productsDiv = By.xpath("//div[@class='products']");
    private final By incrementButtonFirst = By.cssSelector("a.increment");
    private final By addToCart = By.xpath("//button[contains(text(), 'ADD TO CART')]");

    public void waitUntilLandingPageIsLoaded() {
        FluentWait<WebDriver> wait = returnFluentWait(driver, 3, 300);
        var productPageDiv = driver.findElement(productsDiv);
        wait.until(ExpectedConditions.visibilityOf(productPageDiv));

    }
    public void enterSearchItem(String item) {
        driver.findElement(searchField).sendKeys(item);
        testContextSetup.genericUtils.staticWaitFor(2000);  //wait for results to filter - could wait for number of expected returned results
    }

    public void clickTopDealsButton() {
        driver.findElement(topDealsButton).click();
    }

    public String returnProductName() {
        var productNameText = driver.findElement(productName).getText();
        System.out.printf("Product name is: %s%n", productNameText);
        return productNameText;
    }

    public void incrementNumOfFirstItem(int numOfItem) {
        for(int i = 1; i < numOfItem; i++) {
            driver.findElement(incrementButtonFirst).click();
        }
    }

    public void clickAddToCartOnFirstItem() {
        driver.findElement(addToCart).click();
    }

    public FluentWait<WebDriver> returnFluentWait(WebDriver driver, int durationInSec, int pollingInMs) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(durationInSec))
                .pollingEvery(Duration.ofMillis(pollingInMs))
                .ignoring(NoSuchElementException.class);
    }
}